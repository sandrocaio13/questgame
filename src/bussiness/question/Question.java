/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bussiness.question;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author sandro.oliveira && ggg_liberato
 */
public class Question {
    
    private String question;
    private String answer;
    private List<String> alternative;
    private Integer level;
    private String hints;
    
    public Question(String question, String answer, Integer level, String hints,
        List<String> alternatives) {
        this.question = question;
        this.answer = answer;
        this.level = level;
        this.hints = hints;
        this.alternative = alternatives;
    }    

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
    
    public String getHints() {
        return hints;
    }
    
    public void setHints(String hints) {
        this.hints = hints;
    }
    
    public List<String> getAlternatives() {
        return alternative;
    }
    
    @Override
    public String toString() {
        StringBuilder object = new StringBuilder();
        object.append("Question: ").append(this.question);
        object.append("\nAnswer: ").append(this.answer);
        object.append("\nLevel: ").append(this.level);
        object.append("\nAlternatives: ").append(Arrays.toString(
                this.alternative.toArray()));
        object.append("\nHints: ").append(this.hints);
        return object.toString();
    }
}
