/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bussiness;

/**
 *
 * @author sandro.oliveira
 */
public interface Player {
    
    public int getNplayer();
    
    public String getName();
    
    public void setName(String name);
    
    public void managementHitsAndMisses(Boolean hits);
    
    public void showPlayer(Boolean show);
    
    public Boolean managementHints();
    
}
