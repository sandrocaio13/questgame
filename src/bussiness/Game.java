/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bussiness;

/**
 *
 * @author sandro.oliveira
 */
public class Game {
    
    private int numberQuestion;
    private int numberHints;
    private int hits;
    private int misses;
    
    public Game() {
        this.numberQuestion = 1;
        this.numberHints = 3;
        this.hits = 0;
        this.misses = 0;
    }

    public int getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public int getMisses() {
        return misses;
    }

    public void setMisses(int misses) {
        this.misses = misses;
    }

    public int getNumberQuestion() {
        return numberQuestion;
    }

    public void setNumberQuestion(int numberQuestion) {
        this.numberQuestion = numberQuestion;
    }

    public int getNumberHints() {
        return numberHints;
    }

    public void setNumberHints(int numberHints) {
        this.numberHints = numberHints;
    }
    
}
