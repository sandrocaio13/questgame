/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bussiness.player;

import bussiness.Game;
import bussiness.Player;
import javax.swing.JOptionPane;

/**
 *
 * @author sandro.oliveira
 */
public class PlayerImpl extends Game implements Player {

    private final int nPlayer;
    private String name;
    
    public PlayerImpl(String name, int nPlayer) {
        this.nPlayer = nPlayer;
        this.name = name;
    }
    
    @Override
    public int getNplayer() {
        return nPlayer;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
  
    
    @Override
    public void managementHitsAndMisses(Boolean hits) {
        if(hits) {
            setHits(getHits() + 1);
        } else {
            setMisses(getMisses() + 1);
        }
    }
    
    @Override
    public void showPlayer(Boolean show) {
        if(show) {
            JOptionPane.showMessageDialog(null, "Vez do jogador: " + name, 
            "Bem vindo ao QuestGame", 2);
        }
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(this.name);
        sb.append("\nHits: ").append(this.getHits());
        sb.append("\nMisses: ").append(this.getMisses());
        return sb.toString();
    }
    
    @Override
    public Boolean managementHints() {
        if(getNumberHints() > 0) {
            setNumberHints(getNumberHints() - 1);
            return true;
        }
        return false;
    }
    
}
