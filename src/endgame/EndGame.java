/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package endgame;

import bussiness.Player;
import java.util.List;
import javax.swing.JOptionPane;
import questgame.Initial;

/**
 *
 * @author sandro.oliveira
 */
public class EndGame {
    
    private List<Player> players;
    
    public EndGame(List<Player> players) {
        this.players = players;
    }
    
    public void saveHistory() {
        StringBuilder sb = new StringBuilder();
        players.forEach(p -> {
            sb.append(p.toString()).append("\n");
        });
        JOptionPane.showMessageDialog(null, sb.toString(), "Fim do jogo", 1);
        new Initial().setVisible(true);
    }
    
}
