/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import bussiness.question.Question;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author sandro.oliveira
 */
public class ManagementFile {
    
    private String row;
    
    public ManagementFile() {}
    
    public List<Question> readFile(String nameFile) throws FileNotFoundException, IOException {
        List<Question> questions = new ArrayList<>(); 
        BufferedReader reader = new BufferedReader(new FileReader(nameFile));
        while((row = reader.readLine()) != null) {
            String[] data = row.split(",");

            questions.add(new Question(
                    data[0],
                    data[1],
                    Integer.valueOf(data[3]),
                    "testes",
                    Arrays.asList(data[2].split(";"))
            ));
        }
        return questions;
    }
    
    
    
}
