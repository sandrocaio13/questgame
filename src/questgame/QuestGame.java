/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questgame;

import bussiness.Player;
import bussiness.player.GameFrame;
import bussiness.question.Question;
import endgame.EndGame;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JOptionPane;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import utils.ManagementFile;

/**
 *
 * @author sandro.oliveira && ggg_liberato
 */
public class QuestGame {
    
    private List<Player> players;
    private List<Question> questions;
    private int questionsAnswer = 1;
    private int questionsSize;
    
    public QuestGame() {
        this.players = new ArrayList<>();
        getQuestions();
    }
    
    public void setGamer(Player player) {
        if(!players.isEmpty()) {
            Player p = getPlayerByName(player.getName());
            if(p == null) {
                this.players.add(player);
                JOptionPane.showMessageDialog(null, "Jogador: " + player.getName()
                + " adicionado");
            } else {
                JOptionPane.showMessageDialog(null, null, 
                    "Jogador já está presente no jogo", 1);
            }
        } else {
            this.players.add(player);
            JOptionPane.showMessageDialog(null, "Jogador: " + player.getName()
                + " adicionado");
        }
        
    }
    
    public List<Player> getPlayers() {
        return players;
    }
    
    public Player getPlayerByName(String name) {
        Player p = null;
        Optional<Player> optional = players.stream()
            .filter(player -> player.getName().toLowerCase().equals(name))
            .findFirst();
        return optional.isPresent() ? optional.get() : null;
    }
    
    public AudioStream playMusic() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        AudioStream as = new AudioStream(new FileInputStream("rick&morty.wav"));
        AudioPlayer.player.start(as);
        return as;
    }
    
    public void stopMusic(AudioStream as) {
        AudioPlayer.player.stop(as);
    }
    
    @Override
    public String toString() {
        StringBuilder stringPlayers = new StringBuilder();
        players.forEach(p -> {
            stringPlayers.append("Jogador").append(p.getNplayer()).append(": ")
                    .append(p.getName());
            stringPlayers.append("\n");
        });
        return stringPlayers.toString();
    }
    
    public void startGame(int numberPlayer, Boolean show, Question q) {
        Player player = players.get(numberPlayer);
        player.showPlayer(show);
        showQuestion(player, q);
    }
    
    public void showQuestion(Player player, Question question) {
        if(questionsAnswer <= questionsSize) {
            if(question != null) {
                GameFrame gf = new GameFrame(this, question, player);
                gf.setVisible(true);
            } else {
                Question q = getOneQuestion();
                GameFrame gf = new GameFrame(this, q, player);
                gf.setVisible(true);
                questionsAnswer++;
            }
        } else {
            EndGame eg = new EndGame(players);
            eg.saveHistory();
        }
    }
    
    public void getNewQuestion(Boolean answer, Player player, Question q) {
        int nPlayer = checkAnswer(answer, player);
        if(nPlayer != player.getNplayer()) {
            startGame(nPlayer, true, q);
        } else {
            startGame(player.getNplayer(), false, null);
        }
    }
    
    public void getQuestions() {
        ManagementFile f = new ManagementFile();
        try {
            questions = f.readFile("Questions.csv");
            questionsSize = questions.size();
        } catch (IOException ex) {
            Logger.getLogger(QuestGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    public Question getOneQuestion() {
        Random random = new Random();
        int numberQuestion = random.nextInt(questions.size());
        Question q = questions.get(numberQuestion);
        questions.remove(q);
        return q;
    }
    
    public Integer checkAnswer(Boolean answer, Player player) {
        if(answer) {
            players.get(player.getNplayer()).managementHitsAndMisses(answer);
            return player.getNplayer();
        }
        return player.getNplayer() == 0 ? 1 : 0;
    }
    
}
